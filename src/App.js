import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './Home';
import ComicPage from './ComicPage';
import CharacterPage from './CharacterPage';

function App() {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route exact path="/character/:comicId" component={CharacterPage} />
          <Route exact path="/comic/:characterId" component={ComicPage} />
        </Switch>
      </BrowserRouter>
    </>
  );
}

export default App;
