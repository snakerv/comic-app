import React from 'react';
import { POSTER_SIZE } from '../helpers';
import './CharacterInfo.css';

const CharacterInfo = props => {
  const { comic } = props;
  const images =
    comic &&
    `${comic.thumbnail.path}/${POSTER_SIZE}.${comic.thumbnail.extension}`;
  const background = comic && comic.description;

  console.log(comic);
  return (
    <div className="topInfos">
      {comic && <h1>Character Name : {comic.name}</h1>}
      <div>
        <img className="imageCharacter" src={images} alt="test" />
      </div>
      {background ? (
        <h3>Résumé : {background} </h3>
      ) : (
        <h3>Pas de résumé pour ce personnage</h3>
      )}
      {comic && <h2 style={{ textDecoration : 'underline' }}>liste où {comic.name} apparait :</h2>}
    </div>
  );
};

export default CharacterInfo;
