import React, { Component } from "react";
import { API_URL, API_KEY, HASH, BACKDROP_SIZE } from "../helpers";
import CharacterInfo from "../CharacterInfo";
import ThreeColGrid from "../ThreeColGrid";
import ComicThumb from "../ComicThumb";
import Spinner from "../Spinner";
import 'bootstrap/dist/css/bootstrap.css';
import { Button } from 'reactstrap';

class CharacterPage extends Component {
  state = {
    comic: [],
    comicItems: [],
    loading: false,
    heroImage: [],
    currentPage: 0,
    totalPages: 0
  };

  componentDidMount() {
    this.setState({ loading: false });
    const endpoint = `${API_URL}public/characters/${this.props.match.params.comicId}?ts=1&apikey=${API_KEY}&hash=${HASH}`;
    const endpoint2 = `${API_URL}public/characters/${this.props.match.params.comicId}/comics?ts=1&apikey=${API_KEY}&hash=${HASH}&limit=30`;
    this.fetchItems(endpoint);
    this.fetchComicsItems(endpoint2);
  }

   fetchItems = endpoint => {
     fetch(endpoint)
       .then(result => result.json())
       .then(result => {
         this.setState({
           comic: [...this.state.comic, ...result.data.results],
           currentPage: result.data.offset
           //totalPages: result.data.total
         });
         //console.log(result);
       });
   };

  fetchComicsItems = endpoint2 => {
    fetch(endpoint2)
      .then(res => res.json())
      .then(res => {
        this.setState({
          comicItems: [...this.state.comicItems, ...res.data.results],
          totalPages: res.data.total,
          //currentPage: res.data.offset,
          //comic: [...this.state.comic, ...res.data.results]
        });
        console.log(res);
      });
  };

  loadMoreItems = () => {
    let endpointItems = "";
    this.setState({ loading: true });
    endpointItems = `${API_URL}public/characters/${
      this.props.match.params.comicId
    }/comics?ts=1&apikey=${API_KEY}&hash=${HASH}&limit=30&offset=${this.state
      .currentPage + 30}`;
    this.fetchComicsItems(endpointItems);
  };

  render() {
    const { comic } = this.state;
    const { comicItems } = this.state;
    const images = comicItems.map(
      res => `${res.thumbnail.path}/${BACKDROP_SIZE}.${res.thumbnail.extension}`
    );

    return (
      <>
        {/* le composant parent donne ses props a l'enfant */}

        <CharacterInfo comic={comic && comic[0]} />

        <ThreeColGrid>
          {this.state.comicItems.map((i, element) => {
            return (
              <div>
                <ComicThumb
                  clickable={true}
                  key={element}
                  image={images[element]}
                  comicId={i.id}
                />
              </div>
            );
          })}
        </ThreeColGrid>

        {this.state.loading >= this.state.totalPages ? <Spinner /> : null}
        {this.state.currentPage <= this.state.totalPages &&
        !this.state.loading ? (
          <Button color="primary" size="lg" block onClick={this.loadMoreItems}>Load More Marvel comics</Button>
        ) : null}
      </>
    );
  }
}

export default CharacterPage;
