import React from "react";
import { Link } from "react-router-dom";
import "./CharacterThumb.css";

const CharacterThumb = props => {
  console.log(props);
  return (
    <div>
      <Link
        to={{
          pathname: `/character/${props.characterId}`,
          heroName: `/${props.heroName}`
        }}
      >
        <h4 className="characterName">{props.characterName}</h4>
        <img src={props.image} alt={props.characterName} />
      </Link>
    </div>
  );
};

export default CharacterThumb;
