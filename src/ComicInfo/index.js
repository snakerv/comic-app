import React from 'react';
import { POSTER_SIZE, BACKDROP_SIZE } from '../helpers';
import { format } from 'date-fns';
import './comicInfo.css';

const ComicInfo = props => {
  const { comic, creator, prix } = props;
  const images =
    comic &&
    `${comic.thumbnail.path}/${POSTER_SIZE}.${comic.thumbnail.extension}`;
  const background = comic && comic.description;

  const pu = prix && prix.prices.map(m => m.price);

  const saleDate =
    comic && new Date(comic.dates.find(l => l.type === 'onsaleDate').date);
  console.log(saleDate);
  const dateFormat = 'dd/MM/yyyy';
  const dateFormatted = saleDate && format(saleDate, dateFormat);

  const imagesPromo =
    comic &&
    comic.images.map(k => (
      <img
        src={`${k.path}/${BACKDROP_SIZE}.${k.extension}`}
        alt={comic.title}
      ></img>
    ));

  return (
    <>
      <div className="everyInfos">
        <div>
          {comic && <h1>Comic Title : {comic.title}</h1>}
          <div>
            {comic && (
              <img className="comicImage" src={images} alt={comic.title} />
            )}
          </div>

          {background ? (
            <h3>Résumé : {background} </h3>
          ) : (
            <h3>Pas de résumé pour ce comics</h3>
          )}
        </div>

        <div>
          {comic && (
            <div>
              <p>images promotionnelles : </p>
              {imagesPromo}
            </div>
          )}
        </div>

        <div>
          {dateFormatted ? (
            <h3>Date de sortie : {dateFormatted}</h3>
          ) : (
            <h3>Pas d'informations sur la date de sortie</h3>
          )}
        </div>

        <div>
          {pu > 0 ? (
            <h4>A sa sortie, ce comics coutait : {pu[0]} dollars</h4>
          ) : (
            <h4>Aucune information sur le prix de ce comics actuellement</h4>
          )}
        </div>

        <div>
          {comic && comic.pageCount > 0 ? (
            <h3>Ce comics contient {comic.pageCount} pages.</h3>
          ) : (
            <h3>Pas d'information sur le nombre de pages</h3>
          )}
        </div>

        <div>
          {creator ? (
            <h4>créateurs : {creator}</h4>
          ) : (
            <h4>pas de créateurs connus</h4>
          )}
        </div>
      </div>
    </>
  );
};

export default ComicInfo;
