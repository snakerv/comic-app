import React, { Component } from 'react';
import { API_URL, API_KEY, HASH } from '../helpers';
import ComicInfo from '../ComicInfo';

class ComicPage extends Component {
  state = {
    comic: [],
    comicItems: [],
    loading: false,
    heroImage: [],
    creators: [],
  };

  componentDidMount() {
    this.setState({ loading: false });
    const endpoint = `${API_URL}public/comics/${this.props.match.params.characterId}?ts=1&apikey=${API_KEY}&hash=${HASH}`;
    const endpoint2 = `${API_URL}public/comics/${this.props.match.params.characterId}?ts=1&apikey=${API_KEY}&hash=${HASH}&limit=50`;
    this.fetchItems(endpoint);
    this.fetchComicsItems(endpoint2);
  }

  fetchItems = endpoint => {
    fetch(endpoint)
      .then(result => result.json())
      .then(result => {
        this.setState({
          comic: [...this.state.comic, ...result.data.results],
          creators: [...this.state.creators, ...result.data.results],
        });
        console.log(result);
      });
  };

  fetchComicsItems = endpoint2 => {
    fetch(endpoint2)
      .then(res => res.json())
      .then(res => {
        this.setState({
          comicItems: [...this.state.comicItems, ...res.data.results],
        });
        console.log(res);
      });
  };

  render() {
    const { comic } = this.state;

    return (
      <>
        {/* le composant parent donne ses props a l'enfant */}

        <ComicInfo
          comic={comic && comic[0]}
          creator={this.state.creators.map(i => {
            return (
              <div>
                {i.creators.items.map(j => (
                  <h5 key={j.name}>
                    {j.name}, worked as : {j.role}
                  </h5>
                ))}
              </div>
            );
          })}
          prix={comic && comic[0]}
        />
      </>
    );
  }
}

export default ComicPage;
