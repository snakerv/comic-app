import React from 'react';
import { Link } from 'react-router-dom';
import './ComicThumb.css';

const ComicThumb = props => {
  console.log(props);
  return (
    <div>
      <Link
        to={{
          pathname: `/comic/${props.comicId}`,
        }}
      >
        <img src={props.image} alt={props.heroName} />
      </Link>
    </div>
  );
};

export default ComicThumb;
