import React from 'react';
import './Header.css';

const Header = () => {
  return (
    <div className="header-first">
      <p style={{ fontSize: '2em', fontWeight: 'bold' }}>Marvel App</p>
    </div>
  );
};

export default Header;
