import React, { Component } from "react";
import Header from "../Header";
import Searchbar from "../SearchBar";
import CharacterThumb from "../CharacterThumb";
import Spinner from "../Spinner";
import { API_URL, API_KEY, HASH, BACKDROP_SIZE } from "../helpers";
import ThreeColGrid from "../ThreeColGrid";
import 'bootstrap/dist/css/bootstrap.css';
import { Button } from 'reactstrap';

class Home extends Component {
  state = {
    comics: [],
    currentPage: 0,
    totalPages: 0,
    loading: false,
    searchTerm: ""
  };

  componentDidMount() {
    this.setState({ loading: true });
    const endpoint = `${API_URL}public/characters?ts=1&apikey=${API_KEY}&hash=${HASH}&limit=30`;
    this.fetchItems(endpoint);
    console.log(endpoint);
  }

  searchItems = searchTerm => {
    this.setState({ searchTerm, comics: [] });

    const endpoint = `${API_URL}public/characters?ts=1&apikey=${API_KEY}&hash=${HASH}&limit=30&nameStartsWith=${searchTerm}`;
    this.fetchItems(endpoint);
  };

  fetchItems = endpoint => {
    fetch(endpoint)
      .then(result => result.json())
      .then(result => {
        this.setState({
          comics: [...this.state.comics, ...result.data.results],
          currentPage: result.data.offset,
          totalPages: result.data.total,
          loading: false
        });
        console.log(result);
      });
  };

  loadMoreItems = () => {
    let endpointItems = "";
    this.setState({ loading: true });
    if (this.state.searchTerm === "") {
      endpointItems = `${API_URL}public/characters?ts=1&apikey=${API_KEY}&hash=${HASH}&limit=30&offset=${this
        .state.currentPage + 30}`;
    } else {
      endpointItems = `${API_URL}public/characters?ts=1&apikey=${API_KEY}&hash=${HASH}&limit=30&nameStartsWith=${
        this.searchTerm
      }&offset=${this.state.currentPage + 1}`;
    }
    this.fetchItems(endpointItems);
  };

  render() {
    const { comics } = this.state;

    const images = comics.map(
      res => `${res.thumbnail.path}/${BACKDROP_SIZE}.${res.thumbnail.extension}`
    );

    console.table(images);

    return (
      <>
        <Header />
        <Searchbar searchItems={this.searchItems} />

        <ThreeColGrid>
          {this.state.comics.map((i, element) => {
            return (
              <div>
                <CharacterThumb
                  clickable={true}
                  key={i.characterId}
                  image={images[element]}
                  characterId={i.id}
                  characterName={i.name}
                />
              </div>
            );
          })}
        </ThreeColGrid>

        {this.state.loading ? <Spinner /> : null}
        {this.state.currentPage <= this.state.totalPages &&
        !this.state.loading ? (
          // <button onClick={this.loadMoreItems}>
          //   Load More Marvel Characters
          // </button>
          <Button color="primary" size="lg" block onClick={this.loadMoreItems}>Load More Marvel characters</Button>
        ) : null}
      </>
    );
  }
}
export default Home;
