import React, { Component } from 'react';
import FontAwesome from 'react-fontawesome';
import './SearchBar.css';

class SearchBar extends Component {
  handleChange = event => this.props.searchItems(event.currentTarget.value);

  render() {
    console.log(this.value);
    return (
      <div className="rmdb-searchbar">
        <div className="rmdb-searchbar-content">
          <FontAwesome className="rmdb-fa-search" name="search" size="2x" />
          <input
            type="text"
            className="rmdb-searchbar-input"
            placeholder="recherche d'un personnage"
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  }
}

export default SearchBar;
