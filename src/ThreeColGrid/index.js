import React from 'react';
import './ThreeColGrid.css';

const ThreeColGrid = props => {
  const renderElements = () => {
    const gridElements = props.children.map((element, i) => {
      return (
        <div className="rmdb-grid-element" key={i}>
          {element}
        </div>
      );
    });
    return gridElements;
  };

  return (
    <div className="rmdb-grid">
      {<h1>{props.header}</h1>}
      <div className="rmdb-grid-content">{renderElements()}</div>
    </div>
  );
};

export default ThreeColGrid;
