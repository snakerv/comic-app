// liens vers l'api et la clé

const API_URL = 'https://gateway.marvel.com/v1/';
const API_KEY = 'd597a76ba6c82530efb9208e577b8abe';

//private key:

const PRIVATE_KEY = '136bc0230d537fa028a2e0fa4017021f6b72e053';

//md5, ou HASH :

const HASH = 'e3390c3391b5569ccc4849ace01e3118';

// Images

//tailles pour les miniatures
const BACKDROP_SIZE = 'portrait_uncanny';

// taille maximale
const POSTER_SIZE = 'detail';

export { API_URL, API_KEY, BACKDROP_SIZE, POSTER_SIZE, PRIVATE_KEY, HASH };
